﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;


namespace Alien_Run
{
    class Tile
    {
        // ------------------------------------------------
        // Enums
        // ------------------------------------------------
        public enum TileType
        {
            IMPASSABLE, // = 0 blocks the player movement
            PLATFORM, // = 1, Blocks player movement downward only
            SPIKES,   // = 2, Kills the player if land on
            GOAL,     // = 3, way player wins

        }
      
        
        // ------------------------------------------------
        // Data
        // ------------------------------------------------
        private Texture2D sprite;
        private Vector2 position;
        private TileType type;


        // ------------------------------------------------
        // Behaviour
        // ------------------------------------------------
        public Tile(Texture2D newSprite,Vector2 newPosition, TileType newType)
        {
            sprite = newSprite;
            position = newPosition;
            type = newType;
        }
        // ------------------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, position, Color.White);
        }
        // ------------------------------------------------
        public Rectangle GetBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y, sprite.Width,sprite.Height);
        }
        // ------------------------------------------------
        public TileType GetTileType()
        {
            return type;
        }
        // ------------------------------------------------
    }
}
