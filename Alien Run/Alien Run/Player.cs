﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using System;
using Microsoft.Xna.Framework.Input;

namespace Alien_Run
{
    class Player
    {

        // ------------------------------------------------
        // Data
        // ------------------------------------------------
        private Vector2 position = Vector2.Zero;
        private Vector2 prevPosition = Vector2.Zero;
        private Texture2D sprite = null;
        private Vector2 velocity = Vector2.Zero;
        private Level ourLevel = null;
        private bool touchingGround = false;
        private float jumpTime = 0f;
        private bool jumpLaunchInProgress = false;

        private const float MOVE_SPEED = 300.0f;
        private const float GRAVITY_ACCEL = 3400.0f;
        private const float TERMINAL_VEL = 550.0f;
        private const float JUMP_LAUNCH = -1500.0f;
        private const float MAX_JUMP_TIME = 0.3f;
        private const float JUMP_CONTROL_POWER = 0.7f;

        // ------------------------------------------------
        // Behaviour
        // ------------------------------------------------
        public Player(Level newLevel)
        {
            ourLevel = newLevel;
        }
        // ------------------------------------------------
        public void LoadContent(ContentManager content)
        {
            sprite = content.Load<Texture2D>("player/player-stand");
        }
        //--------------------------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(sprite, position, Color.White);
        }
        //--------------------------------------------------

        public void Update(GameTime gameTime)
        {
            if(ourLevel.GetCompleteLevel() == true)
            {
                return;
            }
            float deltaTime = (float)gameTime.ElapsedGameTime.TotalSeconds;

            // Update our velocity based on input, gravity, etc
            // Horizontal velocity is always constant because this is an auto-run game
            velocity.X = MOVE_SPEED;
            // Apply acceleration due to gravity
            velocity.Y += GRAVITY_ACCEL * deltaTime;
            // Clamp our vertical velocity to a terminal range
            velocity.Y = MathHelper.Clamp(velocity.Y, -TERMINAL_VEL, TERMINAL_VEL);

            Input(gameTime);

            // pos2 = pos1 + deltaPos
            // deltaPos = velocity * deltaTime
            prevPosition = position;
            position += velocity * deltaTime;

            // CHeck if we are colliding with anything
            CheckTileCollision();

        }
        //--------------------------------------------------
        
        private void CheckTileCollision()
        {
            // start by say we are not touching ground
            touchingGround = false;
            // Use the player's bounding box to get a list of tiles that we are colliding with.
            Rectangle playerBounds = GetBounds();
            Rectangle prevPlayerBounds = GetPrevBounds();
            // How to get list of tiles? Ask level!
            List<Tile> collidingTiles = ourLevel.GetTilesInBounds(playerBounds);

            // For each colliding tile, move ourselves out of the tile
            foreach (Tile collidingTile in collidingTiles)
            {

                
                // Determine how far we are overlapping the other tile
                Rectangle tileBounds = collidingTile.GetBounds();
                Vector2 depth = GetCollisionDepth(tileBounds, playerBounds);

                Tile.TileType tileType = collidingTile.GetTileType();

                // Only resolve collision if there actually was one
                // depth will be Vector2.Zero if there was no collision
                if (depth != Vector2.Zero)
                {
                    if(tileType == Tile.TileType.SPIKES)
                    {
                        ourLevel.SetupLevel();
                        position = Vector2.Zero;
                        prevPosition = Vector2.Zero;
                        velocity = Vector2.Zero;
                        jumpTime = 0;
                        jumpLaunchInProgress = false;
                        // stop everything
                        return;
                    }
                    else if(tileType == Tile.TileType.GOAL)
                    {
                        ourLevel.CompleteLevel();
                        return;
                    }
                    float absDepthX = Math.Abs(depth.X);
                    float absDepthY = Math.Abs(depth.Y);

                    // Resolve the collision along the shallow axis, as that is the one
                    // we are closest to the edge of and therefore easier to "squeeze out"
                    // Or you can think of it as we only just overlapped on that side


                    if (absDepthY < absDepthX)
                    {
                        bool fallingOntoTile = playerBounds.Bottom > tileBounds.Top && prevPlayerBounds.Bottom <= tileBounds.Top;

                        if(tileType == Tile.TileType.IMPASSABLE || (tileType == Tile.TileType.PLATFORM && fallingOntoTile))
                        {
                            // Y is our shallow axis
                            // Resolve the collision along the Y axis
                            position.Y += depth.Y;

                            // Recalculate bounds for future collision checking
                            playerBounds = GetBounds();

                            // only if feet are below the ground
                            // then jumping is allowed
                            if (playerBounds.Bottom >= tileBounds.Top)
                            {
                                touchingGround = true;
                            }
                        }


                    }
                    // only handle left/right colllision if this is an Impassable tile
                    else if(tileType == Tile.TileType.IMPASSABLE)
                    {
                        // X is our shallow axis
                        // Resolve the collision along the X axis
                        position.X += depth.X;


                        // Recalculate bounds for future collision checking
                        playerBounds = GetBounds();
                    }
                }
            }
        }
        //--------------------------------------------------
        private Rectangle GetBounds()
        {
            return new Rectangle((int)position.X, (int)position.Y,sprite.Width, sprite.Height);

        }

        private Rectangle GetPrevBounds()
        {
            return new Rectangle((int)prevPosition.X, (int)prevPosition.Y, sprite.Width, sprite.Height);

        }
        //--------------------------------------------------
        private Vector2 GetCollisionDepth(Rectangle tile, Rectangle player)
        {
            // This function calculates how far our rectangles are overlapping

            // Calculate the half sizes of both rectangles
            float halfWidthPlayer = player.Width / 2.0f;
            float halfHeightPlayer = player.Height / 2.0f;
            float halfWidthTile = tile.Width / 2.0f;
            float halfHeightTile = tile.Height / 2.0f;

            // Calculate the centers of each rectangle
            Vector2 centrePlayer = new Vector2(player.Left + halfWidthPlayer,
                                                player.Top + halfHeightPlayer);
            Vector2 centreTile = new Vector2(tile.Left + halfWidthTile,
                                             tile.Top + halfHeightTile);


            // How far away are the centres of each of these rectangles from eachother
            float distanceX = centrePlayer.X - centreTile.X;
            float distanceY = centrePlayer.Y - centreTile.Y;

            // Minimum distance these need to be to NOT collide / intersect
            // If EITHER the X or the Y distance is greater than these minima, these are NOT intersecting
            float minDistanceX = halfWidthPlayer + halfWidthTile;
            float minDistanceY = halfHeightPlayer + halfHeightTile;

            // If we are not intersecting at all, return (0,0)
            if (Math.Abs(distanceX) >= minDistanceX || Math.Abs(distanceY) >= minDistanceY)
            {
                return Vector2.Zero;
            }

            // Calculate and return the intersection depth
            // Essentially, how much over the minimum intersection distance are we in each direction
            // AKA by how much are they intersecting in that direction
            float depthX = 0;
            float depthY = 0;

            if (distanceX > 0)
                depthX = minDistanceX - distanceX;
            else
                depthX = -minDistanceX - distanceX;
            if (distanceY > 0)
                depthY = minDistanceY - distanceY;
            else
                depthY = -minDistanceY - distanceY;

            return new Vector2(depthX, depthY);

        }
        //--------------------------------------------------
        private void Input(GameTime gameTime)
        {
            KeyboardState keyBoardState = Keyboard.GetState();

            bool allowedToJump = touchingGround == true || (jumpLaunchInProgress == true && jumpTime <= MAX_JUMP_TIME);

            if (keyBoardState.IsKeyDown(Keys.Space) &&allowedToJump == true)
            {
                jumpLaunchInProgress = true;
                jumpTime += (float)gameTime.ElapsedGameTime.TotalSeconds;

                //velocity.Y = JUMP_LAUNCH ;
                // scale the jump to how much time the space key has been held
                velocity.Y = JUMP_LAUNCH * (1.0f - (float)Math.Pow(jumpTime / MAX_JUMP_TIME, JUMP_CONTROL_POWER));
            }
            else
            {
                jumpLaunchInProgress = false;
                jumpTime = 0.0f;
            }
        }
        //--------------------------------------------------
        public Vector2 GetPosition()
        {
            return position;
        }
        // ------------------------------------------------
        private void KillPlayer()
        {
            ourLevel.SetupLevel();
            position = Vector2.Zero;
            prevPosition = Vector2.Zero;
            velocity = Vector2.Zero;
            jumpTime = 0;
            jumpLaunchInProgress = false;
        }
        // ------------------------------------------------

    }
}
