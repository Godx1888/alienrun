﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using System.Collections.Generic;
using System;

namespace Alien_Run
{
    class Level
    {
        // ------------------------------------------------
        // Data
        // ------------------------------------------------
        private Tile[,] tiles;
        private Texture2D boxTexture;
        private Texture2D bridgeTexture;
        private Texture2D spikesTexture;
        private Texture2D flagTexture;

        private bool completedLevel = false;

        private const int LEVEL_WIDTH = 100;
        private const int LEVEL_HEIGHT = 100;


        private const int TILE_WIDTH = 70;
        private const int TILE_HEIGHT = 70;
    
    

        // ------------------------------------------------
        // Behaviour
        // ------------------------------------------------
        public void LoadContent(ContentManager content)
        {
            // creating a single copy of the tile texture that will be used by all tiles

            boxTexture = content.Load<Texture2D>("tiles/box");
            bridgeTexture = content.Load<Texture2D>("tiles/bridge");
            spikesTexture = content.Load<Texture2D>("items/spikes");
            flagTexture = content.Load<Texture2D>("items/coinBronze");

            SetupLevel();
        }
        //--------------------------------------------------
        public void SetupLevel()
        {
            completedLevel = false;
            tiles = new Tile[LEVEL_WIDTH, LEVEL_HEIGHT];
            // first row of boxes
            CreateBox(0, 10);
            CreateBox(1, 10);
            CreateBox(2, 10);
            CreateBox(3, 10);
            CreateBox(4, 10);
            CreateBox(5, 10);
            CreateBox(6, 10);
            CreateBox(7, 10);
            CreateBox(8, 10);
            CreateBox(9, 10);
            CreateBox(10, 10);
            CreateBox(11, 10);

            // second row of boxes
            CreateBox(17, 10);
            CreateBox(18, 10);
            CreateBox(19, 10);
            CreateBox(20, 10);
            CreateBox(21, 10);
            CreateBox(24, 10);
            CreateBox(25, 10);
            CreateBox(28, 10);
            CreateBox(29, 10);
            CreateBox(30, 10);

            // thrid row of boxes
            CreateBox(36, 10);
            CreateBox(39, 10);
            CreateBox(43, 10);
            CreateBox(44, 10);
            CreateBox(45, 10);
            CreateBox(48, 10);
            CreateBox(51, 10);
            CreateBox(53, 10);
            CreateBox(54, 10);
            CreateBox(55, 10);

            // Create platform
            // first row
            CreateBridge(11, 8);
            CreateBridge(12, 8);
            CreateBridge(13, 8);
            CreateBridge(14, 8);
            CreateBridge(15, 8);
            // Second row
            CreateBridge(22, 8);
            CreateBridge(23, 8);
            CreateBridge(26, 8);
            CreateBridge(31, 8);
            CreateBridge(33, 8);
            // thrid row
            CreateBridge(37, 8);
            CreateBridge(41, 8);
            CreateBridge(46, 8);
            CreateBridge(47, 8);
            CreateBridge(56, 8);


            // Create Spikes
            // first row
            CreateSpikes(10, 9);
            CreateSpikes(11, 9);
            // second row
            CreateSpikes(19, 9);
            CreateSpikes(30, 9);
            // thrid row
            CreateSpikes(45, 9);
            CreateSpikes(55, 9);

            // Create coin
            CreateFlag(56,7);

        }
        //--------------------------------------------------
        public void CreateBox(int tileX, int tileY) 
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(boxTexture, tilePosition,Tile.TileType.IMPASSABLE);
            tiles[tileX, tileY] = newTile;
        }
        //--------------------------------------------------
        public void CreateBridge(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(bridgeTexture, tilePosition, Tile.TileType.PLATFORM);
            tiles[tileX, tileY] = newTile;
        }
        public void CreateSpikes(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(spikesTexture, tilePosition, Tile.TileType.SPIKES);
            tiles[tileX, tileY] = newTile;
        }
        public void CreateFlag(int tileX, int tileY)
        {
            Vector2 tilePosition = new Vector2(tileX * TILE_WIDTH, tileY * TILE_WIDTH);
            Tile newTile = new Tile(flagTexture, tilePosition, Tile.TileType.GOAL);
            tiles[tileX, tileY] = newTile;
        }
        public void Draw(SpriteBatch spriteBatch)
        {
            for (int x = 0; x < LEVEL_WIDTH; ++x)
            {
                for (int y = 0; y < LEVEL_HEIGHT; ++y)
                {
                    if (tiles[x, y] != null)
                        tiles[x, y].Draw(spriteBatch);
                }
            }
        }
        //--------------------------------------------------
        public List<Tile> GetTilesInBounds(Rectangle bounds)
        {
            // Create an empty list to fill with tiles
            List<Tile> tilesInBounds = new List<Tile>();

            // Determine the tile coordinate range for this rect
            int leftTile = (int)Math.Floor((float)bounds.Left / TILE_WIDTH);
            int rightTile = (int)Math.Ceiling((float)bounds.Right / TILE_WIDTH) - 1;
            int topTile = (int)Math.Floor((float)bounds.Top / TILE_HEIGHT);
            int bottomTile = (int)Math.Ceiling((float)bounds.Bottom / TILE_HEIGHT) - 1;

            // Loop through this range and add any tiles to the list
            for (int x = leftTile; x <= rightTile; ++x)
            {
                for (int y = topTile; y <= bottomTile; ++y)
                {
                    if (GetTile(x, y) != null)
                        tilesInBounds.Add(tiles[x, y]);
                }
            }

            return tilesInBounds;
        }
        //--------------------------------------------------
        public Tile GetTile(int x, int y)
        {
            if(x < 0 || x >= LEVEL_WIDTH || y <0 || y>= LEVEL_HEIGHT)
            {
                return null;
            }
            return tiles[x, y];
        }
        //--------------------------------------------------
        public void CompleteLevel()
        {
            completedLevel = true;
            
        }
        //--------------------------------------------------
        public bool GetCompleteLevel()
        {
            return completedLevel;
        }
        //--------------------------------------------------


    }
}
